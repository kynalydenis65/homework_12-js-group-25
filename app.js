const buttons = document.querySelectorAll('.btn');

document.addEventListener('keydown', function(event) {
    const keyName = event.key;

    buttons.forEach(button => {
        if (button.textContent === keyName) {
            button.style.backgroundColor = 'blue';
        } else {
            button.style.backgroundColor = 'black';
        }
    });

    console.log(`Натиснута клавіша: ${keyName}`);
});
